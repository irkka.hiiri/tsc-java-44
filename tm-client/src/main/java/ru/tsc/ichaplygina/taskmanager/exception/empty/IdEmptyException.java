package ru.tsc.ichaplygina.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class IdEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Id is empty.";

    public IdEmptyException() {
        super(MESSAGE);
    }

}
