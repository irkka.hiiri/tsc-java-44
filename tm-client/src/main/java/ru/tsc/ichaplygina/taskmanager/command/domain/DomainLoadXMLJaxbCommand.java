package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class DomainLoadXMLJaxbCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load xml jaxb";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from xml file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().loadXMLJaxb(getSession());
    }

}
