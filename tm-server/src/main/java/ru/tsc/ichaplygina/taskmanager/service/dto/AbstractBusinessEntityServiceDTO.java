package ru.tsc.ichaplygina.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IAbstractBusinessEntityServiceDTO;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserServiceDTO;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractBusinessEntityDTO;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

import java.util.List;

public abstract class AbstractBusinessEntityServiceDTO<E extends AbstractBusinessEntityDTO> extends AbstractServiceDTO<E> implements IAbstractBusinessEntityServiceDTO<E> {

    @NotNull
    protected final IUserServiceDTO userService;

    public AbstractBusinessEntityServiceDTO(@NotNull final IConnectionService connectionService, @NotNull final IUserServiceDTO userService) {
        super(connectionService);
        this.userService = userService;
    }

    @Override
    public abstract void add(@NotNull final String userId, @NotNull final String entityName, @Nullable final String entityDescription);

    @Override
    public abstract void clear(final String userId);

    @Nullable
    @Override
    public abstract E completeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E completeByName(@NotNull final String userId, @NotNull final String entityName);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId, @Nullable final String sortBy);

    @Nullable
    @Override
    public abstract E findById(@NotNull final String userId, @Nullable final String entityId);

    @Nullable
    @Override
    public abstract E findByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract String getId(@NotNull final String userId, @NotNull final String entityName);

    @Override
    public abstract long getSize(@NotNull final String userId);

    @Override
    public abstract boolean isEmpty(@NotNull final String userId);

    @Nullable
    @Override
    public abstract E removeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E removeByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E startById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E startByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E updateById(@NotNull final String userId,
                                 @NotNull final String entityId,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription);

    @Nullable
    public abstract E completeByIndex(@NotNull String userId, int index);

    @Nullable
    public abstract String getId(@NotNull String userId, int index);

    @Nullable
    public abstract E removeByIndex(@NotNull String userId, int index);

    @Nullable
    public abstract E startByIndex(@NotNull String userId, int index);

}
