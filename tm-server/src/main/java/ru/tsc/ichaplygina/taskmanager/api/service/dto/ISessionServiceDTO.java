package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

public interface ISessionServiceDTO extends IAbstractServiceDTO<SessionDTO> {

    void closeSession(@NotNull SessionDTO session);

    SessionDTO openSession(@NotNull String login, @NotNull String password);

    void validatePrivileges(@NotNull String userId);

    void validateSession(@Nullable SessionDTO session);
}
