package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabaseDriver();

    @NotNull String getDatabaseHbm2ddlAuto();

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseShowSql();

    @NotNull String getDatabaseSqlDialect();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseUsername();

}
