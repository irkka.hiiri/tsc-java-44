package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Task not found.";

    public TaskNotFoundException() {
        super(MESSAGE);
    }

}
