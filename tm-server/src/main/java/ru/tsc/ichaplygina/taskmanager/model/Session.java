package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static java.lang.System.currentTimeMillis;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public class Session extends AbstractModel implements Cloneable {

    @Nullable
    @Column
    private String signature;

    @Column(name = "time_stamp")
    private long timeStamp = currentTimeMillis();

    @NotNull
    @ManyToOne
    private User user;

    public Session(@NotNull User user) {
        this.user = user;
    }

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @Override
    public String toString() {
        return getId() + " " + user.getId() + "" + timeStamp;
    }

}
