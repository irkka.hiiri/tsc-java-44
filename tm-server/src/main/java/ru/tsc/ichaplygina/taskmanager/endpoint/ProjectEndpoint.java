package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ISessionService;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService(name = "ProjectEndpoint")
public final class ProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final ISessionService sessionService;

    public ProjectEndpoint(@NotNull final IProjectService projectService,
                           @NotNull final IProjectTaskService projectTaskService,
                           @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void clearProjects(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        projectTaskService.clearProjects(session.getUser().getId());
    }

    @WebMethod
    public void completeProjectById(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeById(session.getUser().getId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void completeProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                       @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeByIndex(session.getUser().getId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void completeProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeByName(session.getUser().getId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void createProject(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "projectName") @NotNull final String projectName,
                              @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        projectService.add(session.getUser().getId(), projectName, projectDescription);
    }

    @WebMethod
    public Project findProjectById(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        return projectService.findById(session.getUser().getId(), projectId);
    }

    @WebMethod
    public Project findProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        return projectService.findByIndex(session.getUser().getId(), projectIndex);
    }

    @WebMethod
    public Project findProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        return projectService.findByName(session.getUser().getId(), projectName);
    }

    @WebMethod
    public List<Project> getProjectList(@WebParam(name = "session") @Nullable final Session session,
                                        @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return projectService.findAll(session.getUser().getId(), sortBy);
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectTaskService.removeProjectById(session.getUser().getId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void removeProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectTaskService.removeProjectByIndex(session.getUser().getId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void removeProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectTaskService.removeProjectByName(session.getUser().getId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectById(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startById(session.getUser().getId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startByIndex(session.getUser().getId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startByName(session.getUser().getId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void updateProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId,
                                  @WebParam(name = "projectName") @NotNull final String projectName,
                                  @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.updateById(session.getUser().getId(), projectId, projectName, projectDescription))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void updateProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectIndex") final int projectIndex,
                                     @WebParam(name = "projectName") @NotNull final String projectName,
                                     @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.updateByIndex(session.getUser().getId(), projectIndex, projectName, projectDescription))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
