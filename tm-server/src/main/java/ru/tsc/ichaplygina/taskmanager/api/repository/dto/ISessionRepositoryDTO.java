package ru.tsc.ichaplygina.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import java.util.List;

public interface ISessionRepositoryDTO extends IAbstractRepositoryDTO<SessionDTO> {

    void clear();

    @NotNull
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findById(@NotNull String id);

    long getSize();

    void removeById(@NotNull String id);

}
