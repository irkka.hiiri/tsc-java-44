package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    String findIdByEmail(@NotNull String login);

    @Nullable
    String findIdByLogin(@NotNull String login);

    long getSize();

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);
}
