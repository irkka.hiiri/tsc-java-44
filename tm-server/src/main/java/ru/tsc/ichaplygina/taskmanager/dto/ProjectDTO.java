package ru.tsc.ichaplygina.taskmanager.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

    public ProjectDTO(@NotNull final String name, @NotNull final String userId) {
        super(name, userId);
    }

    public ProjectDTO(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

}
