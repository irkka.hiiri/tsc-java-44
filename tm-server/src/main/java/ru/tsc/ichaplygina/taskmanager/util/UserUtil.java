package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public class UserUtil {

    @NotNull
    public static Role getRole(@NotNull final String role) {
        if (isEmptyString(role)) return Role.USER;
        try {
            return Role.valueOf(role.toUpperCase());
        } catch (@NotNull final IllegalArgumentException e) {
            return Role.USER;
        }
    }

}
